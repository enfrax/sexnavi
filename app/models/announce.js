// app/models/announce.js
var mongoose = require('mongoose');

// define the schema for our announce model

var announceSchema = mongoose.Schema ({
    
    announce: {
        product       : String,  //Reseller/Club or Escort or//Add
        timesub       : {Days: String, Startend:[Date,Date]},
        category      : Array,
        htype         : String,   //Humantype (black, asian, caucasian etc.)
        services      : Array,
        titleannounce : String,
        htitle        : String,   //Humantitle (Working name)
        description   : String,
        prefix        : String,
        phonenumber   : String,
        accept        : Array,    //SmS, Whatsapp, Viber
        website       : String,
        country       : String,
        region        : String,
        postalcode    : String,
        city          : String,
        address       : Array,    //Address[0] = street and housenumber, Address[1] = Addressextra for example 2. floor
        media         : {video: String, pictures: Array},
        verify        : Boolean,
        active        : Boolean,
       // media         : Array,    //media[0] = Video, media[1-..] = pictures
    }

});